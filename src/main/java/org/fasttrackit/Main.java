package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static List<Shop> shops = new ArrayList<>();
    private static Scanner scanner;

    public static void main(String[] args) {
        System.out.println(" --- SnowRider Here! What's The Destination?--- ");
        scanner = new Scanner(System.in);

        List<Shop> shops = showShops();

        int selection;
        do{
            selection = askUserToChooseAShop(scanner);
            if (selection > shops.size()){
                System.out.println("You choose the wrong shop. Please try again.");
            }
        }
        while (selection > shops.size());

        System.out.println("You selected: " + shops.get(selection - 1).getName());
    }

    private static int askUserToChooseAShop(Scanner scanner) {
        System.out.println("Choose a number from above:");
        int selection = scanner.nextInt();
        return selection;
    }

    private static  List<Shop> showShops() {
        Shop SlipBoard = new Shop("SlipBoard","Sibiu");
        Shop Scopus = new Shop("Scopus","Targul-Mures");
        Shop RideSBGarage = new Shop("RideSBGarage","Sibiu");
        Shop ExtremeGear = new Shop("ExtremeGear","Cluj-Napoca");
        Shop SkiShop = new Shop("SkiShop","Brasov");
        Shop SnowShop = new Shop("SnowShop","Sibiu");

        shops.add(SlipBoard);
        shops.add(Scopus);
        shops.add(RideSBGarage);
        shops.add(ExtremeGear);
        shops.add(SkiShop);
        shops.add(SnowShop);

        System.out.println("Choose a Shop from The List:");
        for(int i = 0; i < shops.size(); i++){
            System.out.println((i + 1) + ". " + shops.get(i).getName());
        }
        return shops;
    }
}

// Functionalities
// 1. Present a list of available SkiShops
// 2. Most wanted goodies from a Shop
//   Shops:
//   - Address
//   - Catalog
//   - Rating
//   - Delivery/ Pick-up
//   - Reviews
//    Catalog:
//   - Name of Shop
//   - Catalog has Groups / Sections
//   - Short Descriptions story
//   - Group had different items
// 3. Each shop has a catalog
// 4. Present Locations
// 5. Search for SkiGear
//   - by gear type
//   - by locations
//   - by name