package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;

public class Shop {
    private final String name;
    private final String address;

    private final Catalog catalog;

    private int rating;

    private boolean supportsDelivery;

    private List<Reviews> reviews = new ArrayList<>();

// Constructor
    public Shop(String name, String address) {
        this.name = name;
        this.address = address;
        this.catalog = new Catalog();
        this.supportsDelivery = false;
    }

    public String getName() {
        return name;
    }
}
